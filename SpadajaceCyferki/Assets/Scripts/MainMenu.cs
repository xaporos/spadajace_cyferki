using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private void Start()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1) // czyli w jakiej jestesmy scenie
        {
            GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>().OdtworzDzwiek(1);
        }
        else if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>().OdtworzDzwiek(2);
        }
    }
    public void ZacznijGre()
    {
        SceneManager.LoadScene(3);
    }

    public void ZamknijGre()
    {
        Application.Quit();
    }
    public void PrzejdzDoMenu()
    {
        SceneManager.LoadScene(0);
    }
}
