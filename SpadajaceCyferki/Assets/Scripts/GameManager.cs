using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject btnNumberPrefab;
    [SerializeField] Transform numbersContainer;
    [SerializeField] Text txtTargetValue;
    [SerializeField] Text txtPointNumber;
    [SerializeField] Text txtTimeCounter;

    private Camera mainCamera;
    Coroutine corutine;

    int targetValue = 21;
    int pointNumber = 0;
    float licznikPoczatkowy = 30f;
    float licznik;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        corutine = StartCoroutine(CreateNewNumber());
        txtTargetValue.text = targetValue.ToString();
        licznik = licznikPoczatkowy;
    }

    public int PobierzIlePunktowZostalo()
    {
        return targetValue - pointNumber;
    }

    void ZmienKolorLicznika()
    {
        if(licznik <= 10 && licznik > 5)
        {
            txtTimeCounter.color = Color.yellow;
        } 
        else if(licznik <= 5)
        {
            txtTimeCounter.color = Color.red;
        }
    }

    void OdliczajCzas()
    {
        if(licznik > 0)
        {
            licznik -= Time.deltaTime;
            //txtTimeCounter.text = Mathf.FloorToInt(licznik).ToString();
            txtTimeCounter.text = licznik.ToString("f0");
            ZmienKolorLicznika();
        }
        
    }

    private void Update()
    {
        UpdateUiElements();
        OdliczajCzas();
        SprawdzStatusGry();
    }

    void UpdateUiElements()
    {
        txtPointNumber.text = pointNumber.ToString();
    }

    IEnumerator CreateNewNumber()
    {
      
        GameObject newNumber = Instantiate(btnNumberPrefab, numbersContainer);
        int randomPositionX = Random.Range(-1000, 1000);
        newNumber.GetComponent<RectTransform>().anchoredPosition = new Vector2(randomPositionX, 2000f);

        yield return new WaitForSeconds(2f);
        corutine = StartCoroutine(CreateNewNumber());
        
    }

    public void AddPoints(int points)
    {
        pointNumber += points;
    }

    //void stopFallingNumbers()
    //{
    //    StopCoroutine(corutine);
    //}

    void SprawdzStatusGry()
    {
        if(licznik > 0)
        {
            if(pointNumber == targetValue)
            {
                WinTheGame();
            }
            else if(pointNumber > targetValue)
            {
                GameOver();
            }
        }
        else
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene(2);
    }

    void WinTheGame()
    {
        SceneManager.LoadScene(1);
    }

    public void PrzejdzDoMenu()
    {
        SceneManager.LoadScene(0);
    }
}
