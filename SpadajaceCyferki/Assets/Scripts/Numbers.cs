using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Numbers : MonoBehaviour
{
    RectTransform rt;  
    float predkoscOpadania = 400f;
    float time = 0.5f;

    GameManager gm;

    Animator anim;
    int wybranaLiczba;

    [SerializeField] Sprite spriteSplash;
    [SerializeField] Sprite[] sprityCyferek;

    SoundManager soundM;
    Parzystosc parzystosc; // odwolanie do innego skryptu

    // Start is called before the first frame update
    void Start()
    {
        soundM = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        anim = GetComponent<Animator>();
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        PrzypiszWarto��Cyferki();
        rt = GetComponent<RectTransform>();

        parzystosc = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Parzystosc>(); 

    }

    // Update is called once per frame
    void Update()
    {
        NumberGoDown();
    }

    void PrzypiszWarto��Cyferki()
    {
        int randomNumber = Random.Range(1, 7);
        wybranaLiczba = randomNumber;
        GetComponent<Image>().sprite = sprityCyferek[randomNumber - 1];
       
    }

    void AktywujSplasha()
    {
        GetComponent<Image>().sprite = spriteSplash;
    }

    public void AkcjaPoKliknieciu()
    {
        gm.AddPoints(wybranaLiczba);
        parzystosc.PobierzKliknietaCyferke(wybranaLiczba);
        AktywujSplasha();
        anim.SetTrigger("znikanie");
        ZmienKolorPrzycisku();
        GetComponent<Button>().interactable = false;

        soundM.OdtworzDzwiek(0);

        Destroy(gameObject, time);
    }

    void ZmienKolorPrzycisku()
    {
        ColorBlock blokKolorow = GetComponent<Button>().colors;
        switch (wybranaLiczba)
        {
            case 1:
                blokKolorow.disabledColor = Color.red;
                break;
            case 2:
                blokKolorow.disabledColor = Color.yellow;
                break;
            case 3:
                blokKolorow.disabledColor = Color.green;
                break;
            case 4:
                blokKolorow.disabledColor = new Color(0.92f, 0.41f, 0f, 1f);
                break;
            case 5:
                blokKolorow.disabledColor = new Color(1f, 0f, 1f, 1f);
                break;
            case 6:
                blokKolorow.disabledColor = Color.blue;
                break;
            default:
                blokKolorow.disabledColor = Color.cyan;
                break;
        }
       
        GetComponent<Button>().colors = blokKolorow; 
    }

    void NumberGoDown()
    {
        rt.anchoredPosition -= new Vector2(0f, 1f) * predkoscOpadania * Time.deltaTime;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
